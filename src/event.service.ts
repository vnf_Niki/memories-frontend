import { Injectable, EventEmitter } from '@angular/core';
import { title } from 'process';

@Injectable({
  providedIn: 'root'
})
export class EventService {

  constructor() { }

  messageSuccess = new EventEmitter();
  messageInfo = new EventEmitter();
  messageWarn = new EventEmitter();
  messageError = new EventEmitter();

  updatedValues = new EventEmitter();

  updateGroup = new EventEmitter();

  openImage = new EventEmitter();

  loadingStart = new EventEmitter();
  loadingEnd = new EventEmitter();

  /*
this.events.messageSuccess.emit('');
this.events.messageInfo.emit('');
this.events.messageWarn.emit('');
this.events.messageError.emit('');
  */
}
