import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { UserPage } from './user.page';
import { AvatarModule } from 'primeng/avatar';
import { InputTextModule } from 'primeng/inputtext';
import { ImageCropperModule } from 'ngx-image-cropper';
import { ProgressSpinnerModule } from 'primeng/progressspinner';
import { CheckboxModule } from 'primeng/checkbox';
import { UserPageRoutingModule } from './user-routing.module';
import { TranslateModule } from '@ngx-translate/core';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    UserPageRoutingModule,
    AvatarModule,
    InputTextModule,
    ImageCropperModule,
    ProgressSpinnerModule,
    CheckboxModule,
    TranslateModule
  ],
  declarations: [UserPage],
  providers: []
})
export class UserPageModule { }
