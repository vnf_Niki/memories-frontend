import { AfterViewInit, Component, ElementRef, Input, OnInit, ViewChild } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { Camera, CameraOptions } from '@ionic-native/Camera/ngx';
import { ActionSheetController, ModalController, Platform } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import { Avatar } from 'primeng/avatar';
import { ApiService } from '../api.service';
import { ImagecropPage } from '../imagecrop/imagecrop.page';

@Component({
  selector: 'app-user',
  templateUrl: './user.page.html',
  styleUrls: ['./user.page.scss'],
})
export class UserPage implements OnInit, AfterViewInit {

  @ViewChild(Avatar, { read: ElementRef }) avatar: ElementRef;
  @Input() modalCtrl: ModalController;

  public user: any;
  public color: string;
  public blockScreen = false;
  public userImage;

  public imageEdited: boolean;
  public nameEdited: boolean;

  constructor(
    private api: ApiService,
    private storage: Storage,
    public modalController: ModalController,
    private camera: Camera,
    public actionSheetController: ActionSheetController,
    private platform: Platform,
    private sanitization: DomSanitizer) { }

  async ngOnInit() {
    this.user = await this.storage.get('user');
    if (this.user.image) {
      let help: any = 'data:image/jpg;base64,' + await this.getImageMe();
      this.userImage = this.sanitization.bypassSecurityTrustUrl(help);
    }

    this.imageEdited = false;
    this.nameEdited = false;
  }

  ngAfterViewInit() {
    this.changeColor();
  }

  changeColor() {
    this.nameEdited = true;
    this.color = this.getNameColor();
    this.avatar.nativeElement.children[0].style.background = this.color;
  }

  getNameColor() {
    return '#' + this.intToRGB(this.hashCode(this.user.name));
  }

  hashCode(str) { // java String#hashCode
    var hash = 0;
    for (var i = 0; i < str.length; i++) {
      hash = str.charCodeAt(i) + ((hash << 5) - hash);
    }
    return hash;
  }

  intToRGB(i) {
    var c = (i & 0x00FFFFFF)
      .toString(16)
      .toUpperCase();

    return "00000".substring(0, 6 - c.length) + c;
  }

  async getImageMe() {
    return await this.api.getImageMe();
  }

  dismiss() {
    // using the injected ModalController this page
    // can "dismiss" itself and optionally pass back data
    this.modalCtrl.dismiss({
      'dismissed': true
    });
  }

  async presentCrop(imageURL) {
    let sub = this.platform.backButton.subscribeWithPriority(9999, () => { });

    const modal = await this.modalController.create({
      component: ImagecropPage,
      cssClass: 'modal-createGroup',
      componentProps: {
        'modalCtrl': this.modalController,
        'imageUrl': imageURL
      }
    });

    await modal.present();

    let image = await modal.onDidDismiss();
    sub.unsubscribe();
    this.blockScreen = false;

    if (image.data) {
      this.user.image = image.data;
      this.userImage = image.data;
      this.imageEdited = true;
    }
    return;
  }

  pickImage(sourceType) {
    this.blockScreen = true;
    const options: CameraOptions = {
      quality: 50,
      sourceType: sourceType,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE
    }
    this.camera.getPicture(options).then((imageData) => {
      // imageData is either a base64 encoded string or a file URI
      let croppedImagepath = 'data:image/jpeg;base64,' + imageData;
      this.presentCrop(croppedImagepath)
      // Handle error
    },
      (error) => {
        this.blockScreen = false;
      });
  }

  async selectImage() {
    const actionSheet = await this.actionSheetController.create({
      header: "Select Image source",
      buttons: [{
        text: 'Load from Library',
        handler: () => {
          this.pickImage(this.camera.PictureSourceType.PHOTOLIBRARY);
        }
      },
      {
        text: 'Use Camera',
        handler: () => {
          this.pickImage(this.camera.PictureSourceType.CAMERA);
        }
      },
      {
        text: 'Cancel',
        role: 'cancel'
      }
      ]
    });
    await actionSheet.present();
  }

  async save() {
    let user: any = await this.api.saveUser(this.nameEdited ? this.user.name : null, this.imageEdited ? await this.base64toBlob(await this.user.image, await this.base64MimeType(await this.user.image)) : null);
    await this.storage.set('user', user.lists);

    this.dismiss();
  }

  async base64toBlob(base64, mimeType) {
    const bytes = atob(base64.split(',')[1]);
    const arrayBuffer = new ArrayBuffer(bytes.length);
    const uintArray = new Uint8Array(arrayBuffer);
    for (let i = 0; i < bytes.length; i++) {
      uintArray[i] = bytes.charCodeAt(i);
    }
    return new Blob([arrayBuffer], { type: mimeType });
  }

  async base64MimeType(encoded) {
    var result = null;

    if (typeof encoded !== 'string') {
      return result;
    }

    var mime = encoded.match(/data:([a-zA-Z0-9]+\/[a-zA-Z0-9-.+]+).*,.*/);

    if (mime && mime.length) {
      result = mime[1];
    }

    return result;
  }

}
