import { Component, Input, Output, OnInit, EventEmitter } from '@angular/core';

@Component({
  selector: 'memories-input',
  templateUrl: './inputs.component.html',
  styleUrls: ['./inputs.component.scss'],
})
export class InputsComponent implements OnInit {

  @Input() fields: InputObject[];

  @Output() outputOne = new EventEmitter<any[]>();
  @Output() outputTwo = new EventEmitter<any[]>();
  @Output() outputThree = new EventEmitter<any[]>();
  @Output() outPutError = new EventEmitter<boolean>();

  public FieldType = Fieldtype;

  constructor() { }

  ngOnInit() { }

  public outputEmit(number: number) {
    if (!this.checkInputs()) {
      this.outPutError.emit(false);
      return;
    }
    let emitValues = [];
    this.fields.forEach(el => {
      emitValues.push({ name: el.fieldName, value: el.value });
    });
    switch (number) {
      case OutputNumber.OUTPUTONE: this.outputOne.emit(emitValues);
      case OutputNumber.OUTPUTTWO: this.outputTwo.emit(emitValues);
      case OutputNumber.OUTPUTTHREE: this.outputThree.emit(emitValues);
    }
  }

  public checkInputs(): boolean {
    this.fields.forEach(el => {
      if (el.required && !el.value && el.value !== '')
        return false;
      if (el.minLength && el.value.length < el.minLength)
        return false;
      if (el.maxLength && el.value.length > el.maxLength)
        return false;
    });
    return true;
  }
}

export class InputObject {
  public action: OutputNumber;
  public fieldName: string;
  public fieldPlaceholder: string;
  public fieldType: Fieldtype;
  public maxLength: number;
  public minLength: number;
  public required: boolean;
  public value: any;
  public width: string;
}

export enum Fieldtype {
  TEXT,
  EMAIL,
  NUMBER,
  PASSWORD,
  BUTTON
}

export enum OutputNumber {
  OUTPUTONE,
  OUTPUTTWO,
  OUTPUTTHREE
}