import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';
import { InputsComponent } from './inputs.component';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule
  ],
  declarations: [InputsComponent],
  providers: [],
  exports: [
    InputsComponent
  ]
})
export class InputsModule { }
