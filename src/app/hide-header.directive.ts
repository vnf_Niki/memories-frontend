import { Directive, Input, ElementRef, Renderer2 } from '@angular/core';

@Directive({
  selector: '[hide-header]', // Attribute selector
  host: {
    '(ionScroll)': 'onContentScroll($event)'
  }
})

export class HideHeaderDirective {
  @Input("header") header: HTMLElement;
  @Input("footer") footer: HTMLElement;
  constructor(public element: ElementRef, public renderer: Renderer2) {
    console.log('Hello HideHeaderDirective Directive');
  }
  ngOnInit() {
    this.renderer.setStyle(this.header, 'webkitTransition', 'top 700ms');
    this.renderer.setStyle(this.footer, 'webkitTransition', 'bottom 700ms');
  }
  onContentScroll(event) {
    if (event.directionY == "down") {
      this.renderer.setStyle(this.header, 'top', '-56px');
      this.renderer.setStyle(this.footer, 'bottom', '-56px');
    }
    else {
      this.renderer.setStyle(this.header, 'top', '0px');
      this.renderer.setStyle(this.footer, 'bottom', '0px');
    }
  }
}