import { Component, OnDestroy, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Platform } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import { Subscription } from 'rxjs';
import { EventService } from 'src/event.service';
import { ApiService } from '../api.service';
import { Fieldtype, InputObject, OutputNumber } from '../inputs/inputs.component';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit, OnDestroy {

  init = true;
  login = false;
  register = false;
  forgot = false;
  recover = false;

  pSubscription: Subscription

  loginFields: InputObject[] = [{
    action: null,
    fieldName: 'email',
    fieldPlaceholder: 'email',
    fieldType: Fieldtype.EMAIL,
    maxLength: null,
    minLength: null,
    required: true,
    value: null,
    width: '100%'
  }, {
    action: null,
    fieldName: 'password',
    fieldPlaceholder: 'password',
    fieldType: Fieldtype.PASSWORD,
    maxLength: 20,
    minLength: 5,
    required: true,
    value: null,
    width: '100%'
  }, {
    action: OutputNumber.OUTPUTONE,
    fieldName: 'login',
    fieldPlaceholder: null,
    fieldType: Fieldtype.BUTTON,
    maxLength: null,
    minLength: null,
    required: null,
    value: null,
    width: '100%'
  }];

  constructor(
    private api: ApiService,
    private events: EventService,
    private platform: Platform,
    private router: Router,
    private storage: Storage) {

    this.pSubscription = this.platform.backButton.subscribeWithPriority(10, () => {
      this.allFalse();
    });
  }

  log(event) {
    console.log(event);

  }

  async ngOnInit() {
    try {
      if (await this.storage.get('authToken')) {
        this.api.setHeaders(await this.storage.get('authToken'));
        let user: any = await this.api.getMe();
        if (user)
          this.router.navigate(['home']);
        else
          this.events.loadingEnd.emit();
      } else
        this.events.loadingEnd.emit();
    } catch (error) {
      this.events.loadingEnd.emit();
    }
  }

  ngOnDestroy() {
    this.pSubscription.unsubscribe();
  }

  changePage(page) {
    this.allFalse()
    switch (page) {
      case 'login': this.login = true; this.init = false; break;
      case 'register': this.register = true; this.init = false; break;
      case 'forgot': this.forgot = true; this.init = false; break;
      case 'recover': this.recover = true; this.init = false; break;
      default: this.allFalse(); break;
    }
  }

  allFalse() {
    this.init = true;
    this.login = false;
    this.register = false;
    this.forgot = false;
    this.recover = false;
  }

  async registerCall(name: string, email: string, code: string, password: string) {
    try {
      this.events.loadingStart.emit();
      if (!this.validateEmail(email))
        throw { error: { message: 'Enter AN Email Adress' } };
      let user: any = await this.api.register(name, email, code, password);
      if (user.lists && user.lists.email === email) {
        this.loginCall(email, password);
        this.events.messageInfo.emit(user.lists);
      }
    } catch (error) {
      this.events.loadingEnd.emit();
    }

  }

  async loginCall(email, password) {
    this.events.loadingStart.emit();
    try {
      if (!this.validateEmail(email))
        throw { error: { message: 'Enter AN Email Adress' } };
      let user: any = await this.api.login(email, password);
      if (user.lists) {
        await this.storage.set('authToken', user.lists.authToken);
        await this.api.setHeaders(user.lists.authToken);
        this.router.navigate(['home']);
        this.allFalse();
        this.events.messageInfo.emit(user.lists);
      }
    } catch (error) {
      this.events.loadingEnd.emit();
    }
  }

  async forgotCall(email) {
    try {
      this.events.loadingStart.emit();
      if (!this.validateEmail(email))
        throw { error: { message: 'Enter AN Email Adress' } };
      let user: any = await this.api.forgot(email);
      if (user.lists) {
        this.changePage('recover');
        this.events.messageInfo.emit(user.lists);
      }
    } catch (error) {
      this.events.loadingEnd.emit();
    }
  }
  async recoverCall(email, code, password) {
    try {
      this.events.loadingStart.emit();
      if (!this.validateEmail(email))
        throw { error: { message: 'Enter AN Email Adress' } };
      let user: any = await this.api.recover(code, email, password);
      if (user.lists && user.lists.email === email) {
        this.loginCall(email, password);
        this.events.messageInfo.emit(user.lists);
      }
    } catch (error) {
      this.events.loadingEnd.emit();
    }
  }

  private validateEmail(email) {
    const re = new RegExp(/^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/);
    return re.test(String(email).toLowerCase());
  }
}
