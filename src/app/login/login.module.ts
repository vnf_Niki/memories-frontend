import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { LoginPage } from './login.page';
import { ApiService } from '../api.service';
import { HttpClientModule } from '@angular/common/http';
import { LoginPageRoutingModule } from './login-routing.module';
import { InputsComponent } from '../inputs/inputs.component';
import { InputsModule } from '../inputs/inputs.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    LoginPageRoutingModule,
    HttpClientModule,
    InputsModule
  ],
  declarations: [
    LoginPage
  ],
  providers: [ApiService]
})
export class LoginPageModule { }
