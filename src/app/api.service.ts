import { Injectable, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Storage } from '@ionic/storage';

@Injectable({
  providedIn: 'root'
})
export class ApiService implements OnInit {

  public url = 'http://' + environment.api.url + '/';

  private headers: HttpHeaders;

  constructor(
    private http: HttpClient,
    private storage: Storage) {
    this.headers = new HttpHeaders();
    this.headers = this.headers.set('Access-Control-Allow-Origin', '*')
    this.setHeaders(null);
  }

  async ngOnInit() {

  }

  async setHeaders(token: any) {
    if (token)
      this.headers = this.headers.set('auth-token', token);
    else if (sessionStorage.getItem('auth-token'))
      this.headers = this.headers.set('auth-token', await this.storage.get('authToken'));
  }

  async login(email: string, password: string) {
    const url = this.url + environment.api.user + '/login';
    return await new Promise((resolveP, reject) => {
      this.http.post(url, { 'email': email, 'password': password }, { headers: this.headers }).toPromise().then(

        (res) => {

          resolveP(res);

        }).catch((err) => {
          reject(err);
        });
    });
  }

  async register(name: string, email: string, code: string, password: string) {
    const url = this.url + environment.api.user + '/register';
    return await new Promise((resolveP, reject) => {
      this.http.post(url, { 'name': name, 'email': email, 'inviteCode': code, 'password': password }, { headers: this.headers }).toPromise().then(

        (res) => {

          resolveP(res);

        }).catch((err) => {
          reject(err);
        });
    });
  }

  async forgot(email: string) {
    const url = this.url + environment.api.user + '/forgotPassword';
    return await new Promise((resolveP, reject) => {
      this.http.post(url, { email }, { headers: this.headers }).toPromise().then(

        (res) => {

          resolveP(res);

        }).catch((err) => {
          reject(err);
        });
    });
  }

  async recover(code: string, email: string, password: string) {
    const url = this.url + environment.api.user + '/resetPassword';
    return await new Promise((resolveP, reject) => {
      this.http.post(url, { code, email, password }, { headers: this.headers }).toPromise().then(

        (res) => {

          resolveP(res);

        }).catch((err) => {
          reject(err);
        });
    });
  }

  async getMe() {
    const url = this.url + environment.api.user;
    return await new Promise((resolveP, reject) => {
      this.http.get(url, { headers: this.headers }).toPromise().then(

        (res) => {

          resolveP(res);

        }).catch((err) => {
          reject(err);
        });
    });
  }

  async verify() {
    const url = this.url + environment.api.user + '/verify';
    return await new Promise((resolveP, reject) => {
      this.http.get(url, { headers: this.headers }).toPromise().then(

        (res) => {

          resolveP(res);

        }).catch((err) => {
          reject(err);
        });
    });
  }

  async getCode(): Promise<any> {
    const url = this.url + environment.api.invite + '/new';
    return await new Promise((resolveP, reject) => {
      this.http.post(url, {}, { headers: this.headers }).toPromise().then(

        (res) => {

          resolveP(res);

        }).catch((err) => {
          reject(err);
        });
    });
  }

  async verifyAccount() {
    const url = this.url + environment.api.verify + '/new';
    return await new Promise((resolveP, reject) => {
      this.http.post(url, {}, { headers: this.headers }).toPromise().then(

        (res) => {

          resolveP(res);

        }).catch((err) => {
          reject(err);
        });
    });
  }

  async resolveAccount(code: string) {
    const url = this.url + environment.api.verify + '/resolve';
    return await new Promise((resolveP, reject) => {
      this.http.post(url, { code: code }, { headers: this.headers }).toPromise().then(

        (res) => {

          resolveP(res);

        }).catch((err) => {
          reject(err);
        });
    });
  }

  async getGroup(id: string) {
    const url = this.url + environment.api.group + '?groupId=' + id;
    return await new Promise((resolveP, reject) => {
      this.http.get(url, { headers: this.headers }).toPromise().then(

        (res) => {

          resolveP(res);

        }).catch((err) => {
          reject(err);
        });
    });
  }

  async getGroupWithGroupCount(id: string) {
    const url = this.url + environment.api.group + '/home?groupId=' + id;
    return await new Promise((resolveP, reject) => {
      this.http.get(url, { headers: this.headers }).toPromise().then(

        (res) => {

          resolveP(res);

        }).catch((err) => {
          reject(err);
        });
    });
  }

  async createGroup(name: string, password: string, description: string) {

    const url = this.url + environment.api.group + '/create';
    return await new Promise((resolveP, reject) => {
      this.http.post(url, { name, password, description }, { headers: this.headers }).toPromise().then(

        (res) => {

          resolveP(res);

        }).catch((err) => {
          reject(err);
        });
    });
  }

  async joinGroup(groupId: string, password: string) {

    const url = this.url + environment.api.group + '/join';
    return await new Promise((resolveP, reject) => {
      this.http.post(url, { groupId, password }, { headers: this.headers }).toPromise().then(

        (res) => {

          resolveP(res);

        }).catch((err) => {
          reject(err);
        });
    });
  }

  async leaveGroup(id: string) {

    const url = this.url + environment.api.group + '/leave';
    return await new Promise((resolveP, reject) => {
      this.http.post(url, { groupId: id }, { headers: this.headers }).toPromise().then(

        (res) => {

          resolveP(res);

        }).catch((err) => {
          reject(err);
        });
    });
  }

  async saveUser(name: string, image: Blob) {

    const url = this.url + environment.api.user + '/edit';
    return await new Promise((resolveP, reject) => {
      const formData = new FormData();

      if (name)
        formData.append('newName', name);
      if (image)
        formData.append('profileImage', image, 'profileImage.' + '' + image.type.split('/')[1]);

      this.http.post(url, formData, { headers: this.headers }).toPromise().then(

        (res) => {

          resolveP(res);

        }).catch((err) => {
          reject(err);
        });
    });
  }

  async getImageMe() {
    const url = this.url + environment.api.static + '/image/me';
    return await new Promise((resolveP, reject) => {
      this.http.get(url, { headers: this.headers, responseType: 'text' }).toPromise().then(

        (res) => {

          resolveP(res);

        }).catch((err) => {
          reject(err);
        });
    });
  }

  async getImageUserGroup(groupId, imagePath) {

    const url = this.url + environment.api.static + '/image';
    return await new Promise((resolveP, reject) => {
      this.http.post(url, { groupId, imagePath }, { headers: this.headers, responseType: 'text' }).toPromise().then(

        (res) => {

          resolveP(res);

        }).catch((err) => {
          reject(err);
        });
    });
  }

  async getPosts(groupId, postIds: string[]) {

    const url = this.url + environment.api.post + '/getPosts';
    return await new Promise((resolveP, reject) => {
      this.http.post(url, { groupId, postIds }, { headers: this.headers, responseType: 'text' }).toPromise().then(

        (res) => {

          resolveP(res);

        }).catch((err) => {
          reject(err);
        });
    });
  }

  async postPosts(titel, date, description, nsfw, groupId, images) {
    const url = this.url + environment.api.post + '/create';

    const formData = new FormData();
    for await (const image of images) {
      formData.append('images', image, 'images.' + '' + image.type.split('/')[1]);
    }

    images = await new Promise((resolveP, reject) => {
      this.http.post(url + 'Images?groupId=' + groupId, formData, { headers: this.headers }).toPromise().then(

        (res) => {

          resolveP(res);

        }).catch((err) => {
          reject(err);
        });
    });

    return await new Promise((resolveP, reject) => {
      this.http.post(url, { titel, date, description, nsfw, groupId, 'images': images.lists.list }, { headers: this.headers }).toPromise().then(

        (res) => {

          resolveP(res);

        }).catch((err) => {
          reject(err);
        });
    });
  }
}
