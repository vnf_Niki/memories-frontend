

export class Post {

    constructor() {
        this.titel = "";
        this.date = null;
        this.userRef = "";
        this.description = "";
        this.images = [];
        this.nsfw = false;
        this.groupRef = "";
    }

    titel: string;
    date: Date;
    userRef: string;
    description: string;
    images: string[];
    nsfw: boolean;
    groupRef: string;

    createdAt: Date;
    updatedAt: Date;
}