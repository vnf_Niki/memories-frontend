

export class Invite {

    public Invite() {
        this.code = "";
        this.userRef = "";
    }

    code: string;
    userRef: string;
    readonly createdAt: Date;
}