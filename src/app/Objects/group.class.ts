

export class Group {

    public Group() {
        this.name = "";
        this.password = "";
        this.description = "";
        this.titlebar = [];
        this.usersRefs = [];
        this.postRefs = [];
    }

    name: string;
    password: string;
    description: string;
    titlebar: [];
    usersRefs: string[];
    postRefs: string[];
    createdAt: Date;
    updatedAt: Date;
}