

export class User {

    public User() {
        this.name = "";
        this.password = "";
        this.email = "";
        this.image = "";
        this.groupRefs = [];
        this.postRefs = [];
        this.emailVerified = false;
    }

    name: string;
    password: string
    email: string;
    image: string;
    groupRefs: string[];
    postRefs: string[];
    emailVerified: boolean;

    createdAt: Date;
    updatedAt: Date;
}