import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { EventService } from 'src/event.service';

@Component({
  selector: 'app-landing-page',
  templateUrl: './landing-page.page.html',
  styleUrls: ['./landing-page.page.scss'],
})
export class LandingPagePage implements OnInit {

  constructor(private router: Router, private event: EventService) { }

  ngOnInit() {
    this.event.loadingEnd.emit();
  }

  changePage(page) {
    switch (page) {
      case 'login': this.router.navigate(['login2']); break;
      case 'register': this.router.navigate(['login2']); break;
      default: break;
    }
  }

}
