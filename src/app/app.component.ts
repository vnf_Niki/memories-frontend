import { Component } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { NgxSpinnerService } from 'ngx-spinner';
import { MessageService } from 'primeng/api';
import { environment } from 'src/environments/environment';
import { EventService } from 'src/event.service';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss'],
  providers: [MessageService],
})
export class AppComponent {

  public showSpinner: boolean = false;

  constructor(private messageService: MessageService, private events: EventService, private spinner: NgxSpinnerService, private translate: TranslateService) {

    // if (!environment.production) {
    //   events.messageSuccess.subscribe((res) => { this.msg(res, 'success') });
    //   events.messageInfo.subscribe((res) => { this.msg(res, 'info') });
    //   events.messageWarn.subscribe((res) => { this.msg(res, 'warn') });
    //   events.messageError.subscribe((res) => { this.msg(res, 'error') });
    // }

    events.loadingEnd.subscribe((res) => { this.stopLoading(); });
    events.loadingStart.subscribe((res) => { this.startLoading(); });

    this.events.loadingStart.emit();

    this.initTranslate();
  }

  msg(msg: string, severity: string) {
    // console.log("msg");

    // switch (severity) {
    //   case 'success': this.messageService.add({ severity: 'success', summary: 'Success', detail: msg }); break;
    //   case 'info': this.messageService.add({ severity: 'info', summary: 'Info', detail: msg }); break;
    //   case 'warn': this.messageService.add({ severity: 'warn', summary: 'Warn', detail: msg }); break;
    //   case 'error': this.messageService.add({ severity: 'error', summary: 'Error', detail: msg }); break;
    // }
  }

  addSingle() {
    // this.messageService.add({ severity: 'success', summary: 'Service Message', detail: 'Via MessageService' });
  }

  startLoading() {
    this.showSpinner = true;
    this.spinner.show();
  }

  stopLoading() {
    this.showSpinner = false;
    this.spinner.hide();
  }

  initTranslate() {
    // Set the default language for translation strings, and the current language.
    this.translate.setDefaultLang('en');


    if (this.translate.getBrowserLang() !== undefined) {
      this.translate.use(this.translate.getBrowserLang());
    } else {
      this.translate.use('en'); // Set your language here
    }

  }
}
