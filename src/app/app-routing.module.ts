import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { HomePage } from './home/home.page';
import { LoggedinGuard } from './loggedin.guard';
import { LoginPage } from './login/login.page';
import { Login2Page } from './login2/login2.page';

const routes: Routes = [

  { path: '', redirectTo: '/login', pathMatch: 'full' },
  { path: 'login', component: LoginPage },
  { path: 'home', component: HomePage, canActivate: [LoggedinGuard] },
  {
    path: 'invite',
    loadChildren: () => import('./invite/invite.module').then(m => m.InvitePageModule)
  },
  {
    path: 'settings',
    loadChildren: () => import('./user/user.module').then(m => m.UserPageModule)
  },
  {
    path: 'create-group',
    loadChildren: () => import('./create-group/create-group.module').then(m => m.CreateGroupPageModule)
  },
  {
    path: 'join-group',
    loadChildren: () => import('./join-group/join-group.module').then(m => m.JoinGroupPageModule)
  },
  {
    path: 'imagecrop',
    loadChildren: () => import('./imagecrop/imagecrop.module').then(m => m.ImagecropPageModule)
  },
  {
    path: 'group',
    loadChildren: () => import('./group/group.module').then(m => m.GroupPageModule)
  },
  {
    path: 'login2',
    loadChildren: () => import('./login2/login2.module').then( m => m.Login2PageModule)
  },
  {
    path: 'landing-page',
    loadChildren: () => import('./landing-page/landing-page.module').then( m => m.LandingPagePageModule)
  }

];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
