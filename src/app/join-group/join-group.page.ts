import { Component, Input, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import { EventService } from 'src/event.service';
import { ApiService } from '../api.service';

@Component({
  selector: 'app-join-group',
  templateUrl: './join-group.page.html',
  styleUrls: ['./join-group.page.scss'],
})
export class JoinGroupPage implements OnInit {
  @Input() modalCtrl: ModalController;

  constructor(
    private api: ApiService,
    private storage: Storage,
    private event: EventService) { }

  ngOnInit() {
  }

  async joinGroup(name, pass) {
    await this.api.joinGroup(name, pass);
    this.event.updatedValues.emit();
    this.dismiss();
  }

  dismiss() {
    // using the injected ModalController this page
    // can "dismiss" itself and optionally pass back data
    this.modalCtrl.dismiss({
      'dismissed': true
    });
  }
}
