import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { JoinGroupPage } from './join-group.page';
import { InputTextareaModule } from 'primeng/inputtextarea';
import { InputTextModule } from 'primeng/inputtext';
import { JoinGroupPageRoutingModule } from './join-group-routing.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    JoinGroupPageRoutingModule,
    InputTextModule,
    InputTextareaModule
  ],
  declarations: [JoinGroupPage]
})
export class JoinGroupPageModule {}
