import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { CropperComponent } from 'angular-cropperjs';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'app-imagecrop',
  templateUrl: './imagecrop.page.html',
  styleUrls: ['./imagecrop.page.scss'],
})
export class ImagecropPage implements OnInit {

  @ViewChild('angularCropper') public angularCropper: CropperComponent;
  @Input() modalCtrl: ModalController;

  imgUrl = null;
  config = {
    aspectRatio: 4 / 4,
    dragMode: 'move',
    background: true,
    movable: true,
    rotatable: true,
    scalable: true,
    zoomable: true,
    viewMode: 0,
    checkImageOrigin: true,
    checkCrossOrigin: true
  };
  imageUrl: any = '';

  constructor(private modalController: ModalController, private sanitization: DomSanitizer) { }

  async ngOnInit() {
    fetch(this.imageUrl)
      .then(async (res) => this.imageUrl = await this.sanitization.bypassSecurityTrustStyle(`url(${res.blob()})`));
  }

  rotateRight() {
    this.angularCropper.cropper.rotate(90);
  }

  rotateLeft() {
    this.angularCropper.cropper.rotate(-90);
  }

  returnImage() {
    this.imgUrl = this.angularCropper.cropper.getCroppedCanvas().toDataURL();
    this.modalController.dismiss(this.imgUrl);
  }

  dismiss() {
    // using the injected ModalController this page
    // can "dismiss" itself and optionally pass back data
    this.modalCtrl.dismiss();
  }
}
