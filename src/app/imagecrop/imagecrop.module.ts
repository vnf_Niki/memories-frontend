import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ImagecropPage } from './imagecrop.page';

import { AngularCropperjsModule } from 'angular-cropperjs';

import { Camera } from '@ionic-native/Camera/ngx';
import { File } from '@ionic-native/file/ngx';
import { ImagecropPageRoutingModule } from './imagecrop-routing.module';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ImagecropPageRoutingModule,
    AngularCropperjsModule
  ],
  declarations: [ImagecropPage],
  providers: [
    Camera,
    File
  ]
})
export class ImagecropPageModule { }
