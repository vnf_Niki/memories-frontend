import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ImagecropPage } from './imagecrop.page';

const routes: Routes = [
  {
    path: '',
    component: ImagecropPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ImagecropPageRoutingModule {}
