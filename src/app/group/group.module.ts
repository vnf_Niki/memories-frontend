import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { GroupPage } from './group.page';
import { PostModule } from './GroupModules/post/post.module';
import { AddPostModule } from './GroupModules/add-post/add-post.module';
import { PostSkeletonModule } from './GroupModules/post-skeleton/post-skeleton.module';
import { GroupPageRoutingModule } from './group-routing.module';
import { GroupSettingsModule } from './GroupModules/group-settings/group-settings.module';
import { TableModule } from 'primeng/table';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    GroupPageRoutingModule,
    PostModule,
    AddPostModule,
    PostSkeletonModule,
    GroupSettingsModule,
    TableModule
  ],
  declarations: [GroupPage]
})
export class GroupPageModule { }
