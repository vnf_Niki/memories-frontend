import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PostComponent } from './post.component';
import { AvatarModule } from 'primeng/avatar';
import { FormsModule } from '@angular/forms';
import { InputTextModule } from 'primeng/inputtext';
import { TextareaAutosizeModule } from 'ngx-textarea-autosize';
import { IonicModule } from '@ionic/angular';
import { SkeletonModule } from 'primeng/skeleton';

import { NgxIonicImageViewerModule } from 'ngx-ionic-image-viewer';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  imports: [
    CommonModule,
    IonicModule,
    AvatarModule,
    FormsModule,
    InputTextModule,
    TextareaAutosizeModule,
    SkeletonModule,
    NgxIonicImageViewerModule,
    TranslateModule
  ],
  declarations: [PostComponent],
  exports: [PostComponent]
})
export class PostModule { }
