import { Component, EventEmitter, Input, OnInit, Output, SecurityContext } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { ApiService } from 'src/app/api.service';
import { Post } from 'src/app/Objects/post.class';
import { User } from 'src/app/Objects/user.class';
import { EventService } from 'src/event.service';

@Component({
  selector: 'memories-post-standard',
  templateUrl: './post.component.html',
  styleUrls: ['./post.component.scss'],
})
export class PostComponent implements OnInit {

  @Input() groupId: string;
  @Input() postId: string;
  @Input() postPosition: number;
  @Input() postLength: number;
  @Input() userImages: any;

  @Output() loaded = new EventEmitter<string>();

  public post: Post;
  public user: User;

  public image: any;
  public imagesLoaded: Promise<boolean>;

  public showMore: boolean = false;

  public shortDesc: string = "";
  public shortThresh: number = 8;

  slideOpts = {
    initialSlide: 1,
    speed: 400
  };

  constructor(
    private api: ApiService,
    private sanitizer: DomSanitizer,
    private events: EventService
  ) { }

  async ngOnInit() {
    this.imagesLoaded = Promise.resolve(false);
    this.post = await JSON.parse(await this.api.getPosts(this.groupId, [this.postId]) + '')[0];
    if (this.post.images.length > 0) {
      let help: any = 'data:image/jpg;base64,' + await this.getImageMe();

      this.image = this.sanitizer.bypassSecurityTrustUrl(help);
      this.image = this.sanitizer.sanitize(SecurityContext.URL, this.image);

      this.imagesLoaded = Promise.resolve(true);
    }

    if (this.userImages)
      for await (const user of this.userImages) {
        if (this.post.userRef === user.userId) {
          this.user = user;
        }
      }

    this.shortDesc = this.post.description.replace(/\r?\n|\r/g, ' ')
    this.shortDesc = this.shortDesc.replace(/ .*/, '');
    this.shortThresh = this.shortDesc.length;
    if (this.post.description.length >= this.shortThresh + 1)
      this.shortDesc = this.shortDesc + "...";

    this.loaded.emit("");
  }

  async getImageMe() {
    return await this.api.getImageUserGroup(this.groupId, this.post.images[0]);
  }

  async openViewer() {
    this.events.openImage.emit({ titel: this.post.titel, image: this.image });
  }

  isFirst(): boolean {
    if (this.postPosition === 0)
      return true;
    else
      return false;
  }

  isLast(): boolean {
    if (this.postPosition === this.postLength - 1)
      return true;
    else
      return false;
  }
}
