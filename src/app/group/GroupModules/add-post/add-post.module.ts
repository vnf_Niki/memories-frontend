import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { AddPostComponent } from './add-post.component';
import { InputTextareaModule } from 'primeng/inputtextarea';
import { InputTextModule } from 'primeng/inputtext';
import { CheckboxModule } from 'primeng/checkbox';
import { AvatarModule } from 'primeng/avatar';
import { ImageCropperModule } from 'ngx-image-cropper';
import { ProgressSpinnerModule } from 'primeng/progressspinner';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    InputTextModule,
    InputTextareaModule,
    CheckboxModule,
    AvatarModule,
    ImageCropperModule,
    ProgressSpinnerModule,
    TranslateModule
  ],
  declarations: [AddPostComponent],
  exports: [
    AddPostComponent
  ]
})
export class AddPostModule { }