import { Component, Input, OnInit } from '@angular/core';
import { ActionSheetController, ModalController, Platform } from '@ionic/angular';
import { Camera, CameraOptions } from '@ionic-native/Camera/ngx';
import { ApiService } from 'src/app/api.service';
import { Post } from 'src/app/Objects/post.class';
import { EventService } from 'src/event.service';

@Component({
  selector: 'memories-add-post',
  templateUrl: './add-post.page.html',
  styleUrls: ['./add-post.page.scss'],
})
export class AddPostComponent implements OnInit {

  @Input() groupId: string;
  @Input() slides: any;

  public post: Post = new Post();

  public user: any;
  public color: string;
  public blockScreen = false;
  public userImage;

  public imageEdited: boolean;
  public nameEdited: boolean;

  constructor(
    private api: ApiService,
    public modalController: ModalController,
    private camera: Camera,
    public actionSheetController: ActionSheetController,
    public events: EventService
  ) { }

  ngOnInit() { }

  async save() {
    this.events.loadingStart.emit();
    let post = await this.api.postPosts(this.post.titel, this.post.date, this.post.description, this.post.nsfw, this.groupId, this.userImage ? await [this.base64toBlob(await this.userImage, await this.base64MimeType(await this.userImage))] : null);
    this.post = new Post();
    this.userImage = null;
    this.events.updateGroup.emit();
    this.slides.slideTo(1);
    this.events.loadingEnd.emit();
  }

  pickImage(sourceType) {
    this.blockScreen = true;
    const options: CameraOptions = {
      quality: 50,
      sourceType: sourceType,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE
    }
    this.camera.getPicture(options).then((imageData) => {
      // imageData is either a base64 encoded string or a file URI
      let croppedImagepath = 'data:image/jpeg;base64,' + imageData;
      this.userImage = croppedImagepath
      // Handle error
    },
      (error) => {
        this.blockScreen = false;
      });
  }

  async selectImage() {
    const actionSheet = await this.actionSheetController.create({
      header: "Select Image source",
      buttons: [{
        text: 'Load from Library',
        handler: () => {
          this.pickImage(this.camera.PictureSourceType.PHOTOLIBRARY);
        }
      },
      {
        text: 'Use Camera',
        handler: () => {
          this.pickImage(this.camera.PictureSourceType.CAMERA);
        }
      },
      {
        text: 'Cancel',
        role: 'cancel'
      }
      ]
    });
    await actionSheet.present();
  }

  async base64toBlob(base64, mimeType) {
    const bytes = atob(base64.split(',')[1]);
    const arrayBuffer = new ArrayBuffer(bytes.length);
    const uintArray = new Uint8Array(arrayBuffer);
    for (let i = 0; i < bytes.length; i++) {
      uintArray[i] = bytes.charCodeAt(i);
    }
    return new Blob([arrayBuffer], { type: mimeType });
  }

  async base64MimeType(encoded) {
    var result = null;

    if (typeof encoded !== 'string') {
      return result;
    }

    var mime = encoded.match(/data:([a-zA-Z0-9]+\/[a-zA-Z0-9-.+]+).*,.*/);

    if (mime && mime.length) {
      result = mime[1];
    }

    return result;
  }
}
