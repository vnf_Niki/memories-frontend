import { Component, Input, OnChanges, OnInit } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { ApiService } from 'src/app/api.service';
import { Post } from 'src/app/Objects/post.class';
import { User } from 'src/app/Objects/user.class';

@Component({
  selector: 'memories-post-skeleton',
  templateUrl: './post-skeleton.html',
  styleUrls: ['./post-skeleton.scss'],
})
export class PostSkeletonComponent implements OnInit {

  @Input() isFirst: boolean;
  @Input() isLast: boolean;

  constructor(
    private api: ApiService,
    private sanitizer: DomSanitizer
  ) { }

  async ngOnInit() {
  }
}
