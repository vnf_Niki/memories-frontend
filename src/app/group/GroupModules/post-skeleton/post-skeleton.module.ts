import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PostSkeletonComponent } from './post-skeleton.page';
import { AvatarModule } from 'primeng/avatar';
import { InputTextModule } from 'primeng/inputtext';
import { TextareaAutosizeModule } from 'ngx-textarea-autosize';
import { SkeletonModule } from 'primeng/skeleton';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    AvatarModule,
    FormsModule,
    InputTextModule,
    TextareaAutosizeModule,
    SkeletonModule
  ],
  declarations: [PostSkeletonComponent],
  exports: [PostSkeletonComponent]
})
export class PostSkeletonModule { }
