import { AfterContentChecked, ChangeDetectorRef, Component, Input, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { IonSlides, ModalController, Platform } from '@ionic/angular';
import { ViewerModalComponent } from 'ngx-ionic-image-viewer';
import { interval, Observable, Subscription } from 'rxjs';
import { EventService } from 'src/event.service';
import { ApiService } from '../api.service';

@Component({
  selector: 'app-group',
  templateUrl: './group.page.html',
  styleUrls: ['./group.page.scss'],
})
export class GroupPage implements OnInit, OnDestroy, AfterContentChecked {

  @Input() modalCtrl: ModalController;
  @Input() groupId: string;

  @ViewChild(IonSlides) slides: IonSlides;

  subscription: Subscription;
  pSubscription: Subscription;

  public title: string;

  public group: any;
  public userImages = [];

  public imagesLoaded: Promise<boolean>;

  public loadedPosts: number = 0;

  public imgViewer = { centeredSlides: true, passiveListeners: false, zoom: { enabled: true } };

  public slideOpts = {
    initialSlide: 1,
    speed: 400,
    pagination: false
  };

  constructor(
    private api: ApiService,
    private platform: Platform,
    private cdref: ChangeDetectorRef,
    private event: EventService,
    private modalController: ModalController,
    private router: Router
  ) {
    this.pSubscription = this.platform.backButton.subscribeWithPriority(15, () => {
      this.dismiss();
    });

    event.updatedValues.subscribe((res) => {
      this.updateGroup(undefined);
    });

    event.openImage.subscribe((res) => {
      this.openViewer(res.titel, res.image);
    });
  }

  async ngOnInit() {
    try {
      this.imagesLoaded = Promise.resolve(false);
      this.group = { posts: ["", "", ""] };
      this.group = await this.api.getGroup(this.groupId);

      await this.setSort();

      for await (const user of this.group.userRefsUsers) {
        await this.userImages.push({ userId: user.userId, name: user.name, image: 'data:image/jpg;base64,' + await this.api.getImageUserGroup(this.groupId, user.image) });
      }
      this.userImages = [].concat(this.userImages);

      if (this.group.titlebar.length > 0)
        this.title = this.group.titlebar.title[0];
      else
        this.title = this.group.name;
      const source = interval(10000);
      this.subscription = source.subscribe(val => this.setTitle());
      this.imagesLoaded = Promise.resolve(true);

      this.awaitLoading();

    } catch (error) {
      this.router.navigate(['home']);
      this.event.loadingEnd.emit();
    }
  }

  async awaitLoading() {
    this.loadedPosts++;

    if (this.loadedPosts >= this.group.posts.length)
      this.event.loadingEnd.emit();
  }

  ngAfterContentChecked() {
    this.cdref.detectChanges();
  }

  async updateGroup(event) {
    try {
      this.event.loadingStart.emit();
      this.loadedPosts = 0;
      this.imagesLoaded = Promise.resolve(false);
      this.group = await this.api.getGroup(this.groupId);

      await this.setSort();

      this.imagesLoaded = Promise.resolve(true);
      this.awaitLoading();
      if (event)
        event.target.complete();
    } catch (error) {
      if (event)
        event.target.complete();
      this.event.loadingEnd.emit();
    }
  }

  async dismiss() {
    if (await this.slides.getActiveIndex() !== 1)
      this.slides.slideTo(1)
    else {
      // using the injected ModalController this page
      // can "dismiss" itself and optionally pass back data
      this.modalCtrl.dismiss({
        'dismissed': true
      });
    }
  }

  dismissSlide() {
    this.slides.slideTo(1, 2000)
  }

  async setTitle() {
    for (let i = 0; i < this.group.titlebar.length; i++) {
      if (this.group.titlebar[i].title === this.title) {
        if (this.group.titlebar.length - 1 === i)
          this.title = this.group.titlebar[0].title;
        else
          this.title = this.group.titlebar[i + 1].title;
        break;
      }
    }
  }

  ngOnDestroy() {
    try {
      this.pSubscription.unsubscribe();
    } catch (error) {

    }
    try {
      this.subscription.unsubscribe();
    } catch (error) {

    }
  }

  async setSort() {
    await this.group.posts.reverse();
  }

  async openViewer(titel, image) {
    const modal = await this.modalController.create({
      component: ViewerModalComponent,
      componentProps: {
        src: image,
        scheme: 'dark',
        title: titel,
        swipeToClose: false,
        slideOptions: '{ centeredSlides: true, passiveListeners: false, zoom: { enabled: true } }'
      },
      cssClass: 'ion-img-viewer',
      keyboardClose: true,
      showBackdrop: true
    });

    return await modal.present();
  }
}
