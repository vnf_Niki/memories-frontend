import { Component, Input, OnInit } from '@angular/core';
import { ModalController, ToastController } from '@ionic/angular';
import { Clipboard } from '@ionic-native/clipboard/ngx';
import { Storage } from '@ionic/storage';
import { ApiService } from '../api.service';
import { Router } from '@angular/router';
import { EventService } from 'src/event.service';

@Component({
  selector: 'app-invite',
  templateUrl: './invite.page.html',
  styleUrls: ['./invite.page.scss'],
})
export class InvitePage implements OnInit {
  @Input() modalCtrl: ModalController;

  public code = "_INVITE_CODE_";
  public emailVerified = false;

  public verifyCode: string;

  constructor(
    private clipboard: Clipboard,
    private toastController: ToastController,
    private storage: Storage,
    private api: ApiService,
    private router: Router,
    private events: EventService) { }

  async ngOnInit() {
    this.code = await this.storage.get('inviteCode');
    let user = await this.storage.get('user');
    this.emailVerified = user.emailVerified;
  }

  async verify() {
    this.events.loadingStart.emit();
    let user: any = await this.api.resolveAccount(this.verifyCode);
    if (user.lists.emailVerified) {
      this.emailVerified = true;
      await this.storage.set('user', user.lists);
      this.router.navigate(['home']);
    }
  }

  async sendMail() {
    try {
      await this.api.verifyAccount();
      this.presentToast('Mail Sent!');
    } catch (error) {
      this.presentToast('Mail Not Sent!');
    }
  }

  copyCode() {
    this.clipboard.copy(this.code);
    this.presentToast('Copied To Clipboard!');
  }

  async presentToast(msg) {
    const toast = await this.toastController.create({
      message: msg,
      duration: 2000
    });
    toast.present();
  }

  dismiss() {
    // using the injected ModalController this page
    // can "dismiss" itself and optionally pass back data
    this.modalCtrl.dismiss({
      'dismissed': true
    });
  }

}
