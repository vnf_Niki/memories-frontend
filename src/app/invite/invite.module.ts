import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { InvitePage } from './invite.page';
import { InputTextModule } from 'primeng/inputtext';
import { InvitePageRoutingModule } from './invite-routing.module';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    InvitePageRoutingModule,
    InputTextModule,
    TranslateModule
  ],
  declarations: [InvitePage]
})
export class InvitePageModule {}
