import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Storage } from '@ionic/storage';
import { ApiService } from './api.service';

@Injectable({
  providedIn: 'root'
})
export class LoggedinGuard implements CanActivate {

  constructor(private router: Router, private storage: Storage, private api: ApiService) { }

  async canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot,): Promise<boolean> {

    try {
      if (await this.storage.get('authToken'))
        return Promise.resolve(true);
      else {
        this.router.navigate(['/login']);
        return Promise.resolve(false);
      }
    } catch (error) {
      this.router.navigate(['/login']);
      return Promise.resolve(false);
    }
  }
}