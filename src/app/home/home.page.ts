import { Component, OnInit } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { Router } from '@angular/router';
import { ModalController, Platform, ToastController } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import { EventService } from 'src/event.service';
import { ApiService } from '../api.service';
import { CreateGroupPage } from '../create-group/create-group.page';
import { GroupPage } from '../group/group.page';
import { InvitePage } from '../invite/invite.page';
import { JoinGroupPage } from '../join-group/join-group.page';
import { UserPage } from '../user/user.page';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit {

  iconBar = true;
  delete = [];
  deleteDialog = false;
  public groups: any = [];
  toast;
  public userImage;

  constructor(
    private api: ApiService,
    private router: Router,
    public modalController: ModalController,
    private storage: Storage,
    private event: EventService,
    private toastController: ToastController,
    private platform: Platform,
    private sanitization: DomSanitizer) {

    event.updatedValues.subscribe((res) => {
      this.updateGroups(undefined);
    });
  }


  onPress(i) {
    this.delete[i] = !this.delete[i];
    this.delete = [].concat(this.delete);
    if (!this.deleteDialog)
      this.presentToastWithOptions();
    let isActive = false;
    this.delete.forEach(del => {
      if (del)
        isActive = true;
    });
    if (!isActive)
      this.toast.dismiss();
  }

  async updateValues() {
    try {
      if (this.toast)
        this.toast.dismiss();
      let user: any = await this.api.getMe();
      await this.storage.set('user', user.lists);
      try {
        let code: any = await this.api.getCode();
        await this.storage.set('inviteCode', code.lists.code);
      } catch (error) {
        console.log("not verified yet. cant set code!");
      }
      this.groups = [];
      if (user.lists.groupRefs) {
        this.delete = [];
        for await (const group of user.lists.groupRefs) {
          try {
            this.groups.push(await this.api.getGroupWithGroupCount(group));
            this.delete.push(false);
          } catch (error) { }
        }
        await this.storage.set('groups', this.groups);

        await this.loadImages();
        this.event.loadingEnd.emit();
      }
      this.event.loadingEnd.emit();

    } catch (error) {
      this.event.loadingEnd.emit();
      this.logout();
    }
  }

  async updateGroups(event) {
    try {
      if (this.toast)
        this.toast.dismiss();
      let user: any = await this.api.getMe();
      this.groups = [];
      if (user.lists.groupRefs) {
        this.delete = [];
        for await (const group of user.lists.groupRefs) {
          try {
            this.groups.push(await this.api.getGroupWithGroupCount(group));
            this.delete.push(false);
          } catch (error) { }
        }
        await this.storage.set('groups', this.groups);

        await this.loadImages();
      }
      if (event) {
        event.target.complete();
        this.event.loadingEnd.emit();
      } else
        this.event.loadingEnd.emit();

    } catch (error) {
      event.target.complete();
      this.event.loadingEnd.emit();
      this.logout();
    }
  }

  async loadImages() {
    for (let i = 0; i < this.groups.length; i++) {
      for (let o = 0; o < this.groups[i].userRefsUsers.length; o++) {
        let help: any = 'data:image/jpg;base64,' + await this.api.getImageUserGroup(this.groups[i].id, this.groups[i].userRefsUsers[o].image);
        this.groups[i].userRefsUsers[o].image = this.sanitization.bypassSecurityTrustUrl(help);
      }
    }
  }

  async ngOnInit() {

    await this.api.setHeaders(await this.storage.get('authToken'));

    try {
      await this.api.verify();
    } catch (error) {
      this.logout();
    }

    try {
      await this.updateValues();
      this.event.loadingEnd.emit();
    } catch (error) {
      this.event.loadingEnd.emit();
    }

    this.event.loadingEnd.emit();
  }

  async getImageUser(url) {
    //return await this.api.getImageUser(url);
  }


  async logout() {
    if (this.toast)
      await this.toast.dismiss();
    try {
      await this.storage.remove('authToken');
      await this.storage.remove('user');
      await this.storage.remove('inviteCode');
      await this.storage.remove('groups');
    } catch (error) {
      console.log('logout error!')
    }
    this.api.setHeaders(undefined);
    this.router.navigate(['']);
  }

  async presentInvite() {
    if (this.toast)
      this.toast.dismiss();

    const modal = await this.modalController.create({
      component: InvitePage,
      cssClass: 'modal-invite',
      componentProps: {
        'modalCtrl': this.modalController
      }
    });
    this.iconBar = false;
    await modal.present();

    await modal.onDidDismiss();
    this.iconBar = true;
    return;
  }

  async presentUser() {
    if (this.toast)
      this.toast.dismiss();

    const modal = await this.modalController.create({
      component: UserPage,
      cssClass: 'modal-user',
      componentProps: {
        'modalCtrl': this.modalController
      }
    });
    this.iconBar = false;
    await modal.present();

    await modal.onDidDismiss();

    this.iconBar = true;
    return;
  }

  async presentCreateGroup() {
    if (this.toast)
      this.toast.dismiss();

    const modal = await this.modalController.create({
      component: CreateGroupPage,
      cssClass: 'modal-createGroup',
      componentProps: {
        'modalCtrl': this.modalController
      }
    });
    this.iconBar = false;
    await modal.present();

    await modal.onDidDismiss();
    this.iconBar = true;
    return;
  }

  async presentJoinGroup() {
    if (this.toast)
      this.toast.dismiss();

    const modal = await this.modalController.create({
      component: JoinGroupPage,
      cssClass: 'modal-joinGroup',
      componentProps: {
        'modalCtrl': this.modalController
      }
    });
    this.iconBar = false;
    await modal.present();

    await modal.onDidDismiss();
    this.iconBar = true;
    return;
  }

  async presentGroup(selectedGroup) {

    this.event.loadingStart.emit();

    if (this.toast)
      this.toast.dismiss();

    const modal = await this.modalController.create({
      component: GroupPage,
      cssClass: 'modal-Group',
      componentProps: {
        'modalCtrl': this.modalController,
        'groupId': selectedGroup.id
      }
    });
    this.iconBar = false;
    await modal.present();

    await modal.onDidDismiss();
    this.iconBar = true;
    return;
  }

  getNameColor(name) {
    return '#' + this.intToRGB(this.hashCode(name));
  }

  hashCode(str) { // java String#hashCode
    var hash = 0;
    for (var i = 0; i < str.length; i++) {
      hash = str.charCodeAt(i) + ((hash << 5) - hash);
    }
    return hash;
  }

  intToRGB(i) {
    var c = (i & 0x00FFFFFF)
      .toString(16)
      .toUpperCase();

    return "00000".substring(0, 6 - c.length) + c;
  }

  async presentToastWithOptions() {
    if (this.toast)
      await this.toast.dismiss();

    this.toast = await this.toastController.create({
      message: 'Click to leave the selected Group(s)',
      buttons: [
        {
          side: 'start',
          icon: 'exit',
          text: 'Leave',
          handler: async () => {
            if (this.groups.length === this.delete.length)
              for (let i = 0; i < this.delete.length; i++) {
                if (this.delete[i])
                  await this.api.leaveGroup(this.groups[i].id)
              }
            this.updateValues();
          }
        }, {
          text: 'Cancel',
          role: 'cancel',
          handler: () => {
            for (let i = 0; i < this.delete.length; i++) {
              this.delete[i] = false;
            }
            this.delete = [].concat(this.delete);
          }
        }
      ]
    });
    this.deleteDialog = true;
    await this.toast.present();
    await this.toast.onDidDismiss();
    this.deleteDialog = false;
  }
}
