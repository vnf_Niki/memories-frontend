import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { CreateGroupPage } from './create-group.page';
import { InputTextModule } from 'primeng/inputtext';
import { InputTextareaModule } from 'primeng/inputtextarea';
import { CreateGroupPageRoutingModule } from './create-group-routing.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CreateGroupPageRoutingModule,
    InputTextModule,
    InputTextareaModule
  ],
  declarations: [CreateGroupPage]
})
export class CreateGroupPageModule { }
