import { Component, Input, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import { EventService } from 'src/event.service';
import { ApiService } from '../api.service';

@Component({
  selector: 'app-create-group',
  templateUrl: './create-group.page.html',
  styleUrls: ['./create-group.page.scss'],
})
export class CreateGroupPage implements OnInit {
  @Input() modalCtrl: ModalController;

  constructor(
    private api: ApiService,
    private storage: Storage,
    private event: EventService) { }

  ngOnInit() {
  }

  async createGroup(name, pass, desc) {
    await this.api.createGroup(name, pass, desc);
    this.event.updatedValues.emit();
    this.dismiss();
  }

  dismiss() {
    // using the injected ModalController this page
    // can "dismiss" itself and optionally pass back data
    this.modalCtrl.dismiss({
      'dismissed': true
    });
  }
}
